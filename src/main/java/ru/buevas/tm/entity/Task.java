package ru.buevas.tm.entity;

/**
 * Сущность для хранения инфомации о задаче
 */
public class Task {

    private Long id = System.nanoTime();

    private String name = "";

    private String description = "";

    private Long projectId;

    /**
     * Конструктор по умолчанию
     */
    public Task() {
    }

    /**
     * Конструктор по имени задачи
     *
     * @param name наименование создаваемой задачи
     */
    public Task(final String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(final Long projectId) {
        this.projectId = projectId;
    }
    
    @Override
    public String toString() {
        return String.format("{Task %d - %s}", id, name);
    }
}
