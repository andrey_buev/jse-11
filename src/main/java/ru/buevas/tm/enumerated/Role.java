package ru.buevas.tm.enumerated;

/**
 * Перечисление пользовательских ролей
 */
public enum Role {
    /**
     * Роль для пользователя со стандартными привилегиями
     */
    USER("User"),
    /**
     * Роль для пользователя с привилегиями администратора
     */
    ADMIN("Administrator");

    private final String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String toString() {
        return displayName;
    }
}
