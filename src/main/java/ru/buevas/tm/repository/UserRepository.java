package ru.buevas.tm.repository;

import java.util.ArrayList;
import java.util.List;
import ru.buevas.tm.entity.User;
import ru.buevas.tm.enumerated.Role;

/**
 * Репозиторий пользователей
 *
 * @author Andrey Buev
 */
public class UserRepository {

    /**
     * Коллекция, хранящая пользователей
     */
    private final List<User> users = new ArrayList<>();

    /**
     * Создание нового пользователя
     * При создании автоматически присваеивается роль стандартного пользователя
     *
     * @param login логин
     * @param passwordHash md5-хэш пароля
     * @return созданный пользователь
     */
    public User create(final String login, final String passwordHash) {
        final User user = new User(login, passwordHash, Role.USER);
        users.add(user);
        return user;
    }

    /**
     * Создание нового пользователя
     *
     * @param login логин
     * @param passwordHash md5-хэш пароля
     * @param role роль
     * @return созданный пользователь
     */
    public User create(final String login, final String passwordHash, final Role role) {
        final User user = new User(login, passwordHash, role);
        users.add(user);
        return user;
    }

    /**
     * Получение всех пользователей
     *
     * @return коллекция пользователей
     */
    public List<User> findAll() {
        return users;
    }

    /**
     * Обновление информации о пользователе
     *
     * @param id идентификатор пользователя
     * @param login логин
     * @param passwordHash md5-хэш пароля
     * @param role роль
     * @return обновленный пользователь
     */
    public User update(final Long id, final String login, final String passwordHash, final Role role) {
        final User user = findById(id);
        if (user == null) return null;

        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        user.setRole(role);

        return user;
    }

    /**
     * Получение пользователя по номеру в списке
     *
     * @param index номер в списке
     * @return null если пользоватеь не найден, иначе пользователь
     */
    public User findByIndex(final int index) {
        if (checkIndex(index)) return users.get(index);
        return null;
    }

    /**
     * Получение пользователя по идентификатору
     *
     * @param id идентификатор пользователя
     * @return null если пользоватеь не найден, иначе пользователь
     */
    public User findById(final Long id) {
        for (final User user : users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    /**
     * Получение пользователя по имени
     *
     * @param login логин пользователя
     * @return null если пользоватеь не найден, иначе пользователь
     */
    public User findByLogin(final String login) {
        for (final User user : users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    /**
     * Удаление пользователя по номеру в списке
     *
     * @param index номер в списке
     * @return null если удаляемый пользователь не найдена, иначе удаленный пользователь
     */
    public User removeByIndex(final int index) {
        final User user = findByIndex(index);
        if (user != null) {
            users.remove(user);
            return user;
        }
        return null;
    }

    /**
     * Удаление пользователя по идентификатору
     *
     * @param id идентификатор пользователя
     * @return null если удаляемый пользователь не найдена, иначе удаленный пользователь
     */
    public User removeById(final Long id) {
        final User user = findById(id);
        if (user != null) {
            users.remove(user);
            return user;
        }
        return null;
    }

    /**
     * Удаление пользователя по имени
     *
     * @param login логин пользователя
     * @return null если удаляемый пользователь не найдена, иначе удаленный пользователь
     */
    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user != null) {
            users.remove(user);
            return user;
        }
        return null;
    }

    /**
     * Очистка коллекции пользователей
     */
    public void clear() {
        users.clear();
    }

    /**
     * Проверка индекса на предмет выхода за пределы коллекции пользователей
     *
     * @param index проверяемый индекс
     * @return true - индекс корректен, false - индекс выходит за пределы коллекции
     */
    private boolean checkIndex(final int index) {
        final int lastIndex = users.size() - 1;
        return (index >= 0) && (index <= lastIndex);
    }
}
