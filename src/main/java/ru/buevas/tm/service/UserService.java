package ru.buevas.tm.service;

import java.util.List;
import org.apache.commons.codec.digest.DigestUtils;
import ru.buevas.tm.entity.User;
import ru.buevas.tm.enumerated.Role;
import ru.buevas.tm.repository.UserRepository;

/**
 * Сервис пользователей
 *
 * Делегирует операции над пользователями репозиторию с предшествующей обработкой и валидацией поступающей информации
 *
 * @author Andrey Buev
 * @see UserRepository
 */
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User create(String login, String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;

        return userRepository.create(login, md5Hash(password));
    }

    public User create(String login, String password, Role role) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;

        return userRepository.create(login, md5Hash(password), role);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User update(Long id, String login, String password,
                       Role role) {
        return userRepository.update(id, login, md5Hash(password), role);
    }

    public User findByIndex(int index) {
        return userRepository.findByIndex(index);
    }

    public User findById(Long id) {
        return userRepository.findById(id);
    }

    public User findByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    public User removeByIndex(int index) {
        return userRepository.removeByIndex(index);
    }

    public User removeById(Long id) {
        return userRepository.removeById(id);
    }

    public User removeByLogin(String login) {
        return userRepository.removeByLogin(login);
    }

    public void clear() {
        userRepository.clear();
    }

    private String md5Hash(String data) {
        final String hashedData = DigestUtils.md5Hex(data).toUpperCase();
        return hashedData;
    }
}
